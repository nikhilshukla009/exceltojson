package com.exToJson.exToJson.repository;

import com.exToJson.exToJson.Entity.Leads;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LeadRepository extends MongoRepository<Leads, String> {
}
