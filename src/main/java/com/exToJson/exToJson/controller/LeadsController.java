package com.exToJson.exToJson.controller;

import com.exToJson.exToJson.Entity.Leads;
import com.exToJson.exToJson.service.LeadService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class LeadsController {
    private ObjectMapper objectMapper;
    Leads l;
@Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    List<Leads> leads = new ArrayList<>();

    @Autowired
    private LeadService leadService;




    @GetMapping("/leads")
    public List<Leads> readExcelReturnJson() throws Exception{
        List<Map<String, String>> dataList = new ArrayList<>();

        InputStream inputStream = getClass().getResourceAsStream("/allLeads.xlsx");
        Workbook workbook= new XSSFWorkbook(inputStream);
        Sheet sheet = workbook.getSheetAt(0);
        Row headerRow = sheet.getRow(0);
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum() ; rowIndex++) {
            Row currentRow = sheet.getRow(rowIndex);
            Map<String, String> rowData = new HashMap<>();

            for (int cellIndex = 0; cellIndex < headerRow.getLastCellNum(); cellIndex++) {
                Cell headerCell = headerRow.getCell(cellIndex);
                Cell cell = currentRow.getCell(cellIndex);
//                rowData.put(headerCell.getDateCellValue() , cell.getDateCellValue());
                if(cell!=null && cell.getCellType()== CellType.STRING){
                    rowData.put(String.valueOf(headerCell.getStringCellValue()), String.valueOf(cell.getStringCellValue()));
                }
                if(cell!=null && cell.getCellType()== CellType.NUMERIC){
                    rowData.put(String.valueOf(headerCell.getStringCellValue()), String.valueOf(cell.getNumericCellValue()));
                }
            }
            String dat=objectMapper.writeValueAsString(rowData);
            l = objectMapper.readValue(dat, new TypeReference<Leads>() {});

//            leadService.saveLead(l);
            leads.add(l);
//          return l;
        }
        return leads;
        // My second commit
    }
}
