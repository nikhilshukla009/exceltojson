package com.exToJson.exToJson.service;

import com.exToJson.exToJson.Entity.Leads;
import com.exToJson.exToJson.repository.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LeadServiceImpl implements LeadService{
    @Autowired
    private LeadRepository leadRepository;
    Leads leads = new Leads();


    @Override
    public Leads saveLead(Leads leads) {
        return leadRepository.save(leads);
    }
}
