package com.exToJson.exToJson.Entity;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "leads")
public class Leads {
    private String leadId;
    private String lastName;
    private String firstName;
    private String contactNo;
    private String dateOfBirth;
    private String gender;
    private String city;

    private String monthlyProfit;
    private String utmContent;
    private String clickId;
    private String emailId;
    private String salary;
    private String createdOn;
    private String utmId;
    private String browser;
    private String state;
    private String turnover;

    private String preApprovedAmount;
    private String utmSource;
    private String panCard;
    private String employmentType;
    private String utmTerm;
    private String ipAddress;

    private String userId;
    private String utmCampaign;
    private  String lenderId;
    private String loanAmount;
    private  String salaryMode;

    private String pinCode;
    private String lenderName;
    private String loanTenure;
    private String utmMedium;
    private String maritalStatus;
    private String status;
}
