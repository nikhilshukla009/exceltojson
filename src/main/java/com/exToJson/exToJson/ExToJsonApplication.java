package com.exToJson.exToJson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExToJsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExToJsonApplication.class, args);
	}

}
